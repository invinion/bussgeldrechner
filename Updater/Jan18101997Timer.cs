﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

//Checktime
namespace Jan18101997.Time
{
    /// <summary>
    /// Specifies on whitch change the event raises
    /// </summary>
    public enum CheckOptions
    {
        /// <summary>
        /// Raise on Second Change
        /// </summary>
        CheckSeconds,
        /// <summary>
        /// Raise on Minute Change
        /// </summary>
        CheckMinutes,
        /// <summary>
        /// Raise on Hour Change
        /// </summary>
        CheckHours,
        /// <summary>
        /// Raise on Day Change
        /// </summary>
        CheckDays,
        /// <summary>
        /// Raise the event each pass
        /// </summary>
        TestTicks
    }

    class CheckTimeEvent : EventArgs
    {
        private DateTime time;

        public CheckTimeEvent()
        {
            time = DateTime.Now;
        }

        public DateTime CheckTimeTickEvent
        {
            get { return time; }
            set { time = value; }
        }

        public int TickID { get; set; }

        public CheckOptions Options { get; set; }

        public bool TestTick { get; set; }

        public TimeSpan Interval { get; set; }
    }

    /// <summary>
    /// You can create a simple event to check if the Time changed.
    /// </summary>
    class CheckTime
    {
        #region Constructor
        /// <summary>
        /// Main Constructor for the TimeChecker with all default Settings.
        /// </summary>
        public CheckTime()
        {
            Options = CheckOptions.CheckMinutes;
            checkTime = new TimeSpan(0, 0, 1);
            TickID = 0;
            timeCheckThread = new Thread(MainCheck);
            timeCheckThread.IsBackground = true;
        }
        /// <summary>
        /// Main Constructor for the TimeChecker with some custom Settings
        /// </summary>
        /// <param name="options">Set Options for the Checking. Possibilitys: CheckOptions.CheckSeconds / CheckMinutes / CheckHours / CheckDays / TestTicks</param>
        public CheckTime(CheckOptions options)
        {
            Options = options;
            checkTime = new TimeSpan(0, 0, 1);
            TickID = 0;
            timeCheckThread = new Thread(MainCheck);
            timeCheckThread.IsBackground = true;
        }
        /// <summary>
        /// Main Constructor for the TimeChecker with some custom Settings
        /// </summary>
        /// <param name="zeit">How often the Programm checks a time Change</param>
        public CheckTime(TimeSpan zeit)
        {
            Options = CheckOptions.CheckMinutes;
            checkTime = zeit;
            TickID = 0;
            timeCheckThread = new Thread(MainCheck);
            timeCheckThread.IsBackground = true;
        }
        /// <summary>
        /// Main Constructor for the TimeChecker with all custom Settings
        /// </summary>
        /// <param name="options">Set Options for the Checking. Possibilitys: CheckOptions.CheckSeconds / CheckMinutes / CheckHours / CheckDays / TestTicks</param>
        /// <param name="zeit">How often the Programm checks a time Change</param>
        public CheckTime(CheckOptions options, TimeSpan zeit)
        {
            Options = options;
            checkTime = zeit;
            TickID = 0;
            timeCheckThread = new Thread(MainCheck);
            timeCheckThread.IsBackground = true;
        }
        #endregion

        #region public Variabls
        public event TickHandler TickEvent;
        public delegate void TickHandler(object sender, CheckTimeEvent e);
        /// <summary>
        /// How often the Programm checks if the time changed
        /// </summary>
        public TimeSpan checkTime { get; set; }
        /// <summary>
        /// for what is checked?
        /// </summary>
        public CheckOptions Options { get; set; }
        /// <summary>
        /// How many ticks were raised
        /// </summary>
        public int TickID { get; set; }
        #endregion

        #region private Variabls
        private Thread timeCheckThread;
        private int lastSecond = 0;
        private int lastMinute = 0;
        private int lastHour = 0;
        private int lastDay = 0;
        #endregion

        #region Main "UI" Methods
        /// <summary>
        /// Return a bool to see if the Thread is running.
        /// </summary>
        public bool isAlive()
        {
            return timeCheckThread.IsAlive;
        }
        /// <summary>
        /// To start the Checking Thread
        /// </summary>
        public void startTimeCheck()
        {
            timeCheckThread.Start();
        }
        /// <summary>
        /// To stop the Checking Thread
        /// </summary>
        public void stopTimeCheck()
        {
            timeCheckThread.Abort();
        }
        /// <summary>
        /// Resets the TickID back to 0
        /// </summary>
        public void resetTickID()
        {
            TickID = 0;
        }
        #endregion

        #region Methods
        private void MainCheck()
        {
            while (true)
            {
                lastSecond = DateTime.Now.Second;
                lastMinute = DateTime.Now.Minute;
                lastHour = DateTime.Now.Hour;
                lastDay = DateTime.Now.DayOfYear;

                Thread.Sleep(checkTime);

                switch (Options)
                {
                    case CheckOptions.CheckSeconds:
                        if (lastDay != DateTime.Now.DayOfYear || lastHour != DateTime.Now.Hour || lastMinute != DateTime.Now.Minute || lastSecond != DateTime.Now.Second)
                        {
                            raiseEvent(false);
                        }
                        break;
                    case CheckOptions.CheckMinutes:
                        if (lastDay != DateTime.Now.DayOfYear || lastHour != DateTime.Now.Hour || lastMinute != DateTime.Now.Minute)
                        {
                            raiseEvent(false);
                        }
                        break;
                    case CheckOptions.CheckHours:
                        if (lastDay != DateTime.Now.DayOfYear || lastHour != DateTime.Now.Hour)
                        {
                            raiseEvent(false);
                        }
                        break;
                    case CheckOptions.CheckDays:
                        if (lastDay != DateTime.Now.DayOfYear)
                        {
                            raiseEvent(false);
                        }
                        break;
                    case CheckOptions.TestTicks:
                        raiseEvent(true);
                        break;
                }

                TickID++;
            }
        }

        private void raiseEvent(bool testtick)
        {
            CheckTimeEvent currentTime = new CheckTimeEvent();
            currentTime.Options = Options;
            currentTime.TickID = TickID;
            currentTime.TestTick = testtick;
            currentTime.Interval = checkTime;
            TickEvent(this, currentTime);
        }
        #endregion
    }
}

//StopWatch
namespace Jan18101997.Time
{
    class StopwatchEvent : EventArgs
    {
        public double Time { get; set; }
    }

    /// <summary>
    /// Just a simple StopWatch
    /// </summary>
    class StopWatch
    {
        /// <summary>
        /// Main Constructor for the StopWatch
        /// </summary>
        public StopWatch()
        {
            watch = new Thread(MainWatchMethode);
            watch.IsBackground = true;
        }
        /// <summary>
        /// Main Constructor for the StopWatch
        /// </summary>
        /// <param name="start">Starts the Stopwatch on calling</param>
        public StopWatch(bool start)
        {
            watch = new Thread(MainWatchMethode);
            watch.IsBackground = true;
            watch.Start();
        }

        Thread watch = null;
        public event StopwatchTimeAdded TimeAdded;
        public delegate void StopwatchTimeAdded(object sender, StopwatchEvent e);
        public double Time { get; set; }

        /// <summary>
        /// Starts the StopWatch
        /// </summary>
        public void Start()
        {
            watch.Start();
        }
        /// <summary>
        /// Starts the StopWatch
        /// </summary>
        /// <param name="StartTime">Set the Time where the Programm starts to count</param>
        public void Start(double StartTime)
        {
            Time = StartTime;
            watch.Start();
        }
        /// <summary>
        /// Stops the StopWatch
        /// </summary>
        public double Stop()
        {
            watch.Abort();
            return Time;
        }
        /// <summary>
        /// Resets the StopWatch
        /// </summary>
        public void Reset()
        {
            Time = 0;
        }
        /// <summary>
        /// Returns if the Stopwatch is running
        /// </summary>
        public bool isAllive()
        {
            return watch.IsAlive;
        }

        private void MainWatchMethode()
        {
            while (true)
            {
                Thread.Sleep(10);
                Time = Math.Round(Time, 2) + 0.01;

                StopwatchEvent watch = new StopwatchEvent();
                watch.Time = Time;
                if (TimeAdded != null)
                {
                    TimeAdded(this, watch);
                }
            }
        }
    }
}

//Timer
namespace Jan18101997.Time
{
    class TimerEventArgs : EventArgs
    {
        public TimerEventArgs(int par1, int par2)
        {
            TimeRemaining = par1;
            TimeTotal = par2;
        }

        public int TimeRemaining { get; set; }
        public int TimeTotal { get; set; }
    }

    class NotHandledException : Exception
    {
        public NotHandledException() { }

        public NotHandledException(string message) : base(message) { }
    }

    class Timer
    {
        public Timer()
        {
            Time = new TimeSpan(0, 0, 60);
            timer = new Thread(timerMethode);
            timer.IsBackground = true;
        }

        public Timer(TimeSpan time)
        {
            Time = time;
            timer = new Thread(timerMethode);
            timer.IsBackground = true;
        }

        public Timer(TimeSpan time, bool start)
        {
            Time = time;
            timer = new Thread(timerMethode);
            timer.IsBackground = true;

            if (start)
            {
                Start();
            }
        }


        public TimeSpan Time { get; set; }
        int timeremaining = -1;
        int totalseconds = 0;
        int tick = 0;
        Thread timer;
        public event TimerEventRemaining TimeRemainingEvent;
        public delegate void TimerEventRemaining(object sender, TimerEventArgs e);
        public event TimeEventTimeOver TimeOverEvent;
        public delegate void TimeEventTimeOver(object sender, TimerEventArgs e);

        public int calculateSeconds()
        {
            return (int)Math.Round(Time.TotalSeconds);
        }

        public void Start()
        {
            totalseconds = calculateSeconds();
            timeremaining = 2;
            timer.Start();
        }

        public void Stop()
        {
            timer.Abort();
            timeremaining = -1;
            totalseconds = 0;
            tick = 0;
        }

        public bool isAlive()
        {
            return timer.IsAlive;
        }

        public int TimeRemaining()
        {
            return timeremaining;
        }

        private void timerMethode()
        {
            TimerEventArgs args = new TimerEventArgs(totalseconds, totalseconds);
            if (TimeRemainingEvent != null)
            {
                TimeRemainingEvent(this, args);
            }

            while(timeremaining > 0)
            {
                Thread.Sleep(1000);
                tick++;
                timeremaining = totalseconds - tick;

                args = new TimerEventArgs(timeremaining, totalseconds);
                if (TimeRemainingEvent != null)
                {
                    TimeRemainingEvent(this, args);
                }
            }

            args = new TimerEventArgs(timeremaining, totalseconds);
            if (TimeOverEvent != null)
            {
                TimeOverEvent(this, args);
            }
            else
            {
                throw new NotHandledException("You have to create at least 1 Handler to use this timer the right Way!");
            }
        }
    }
}
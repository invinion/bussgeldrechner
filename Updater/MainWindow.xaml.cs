﻿using Jan18101997.Time;
using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Windows;

namespace Updater
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        string downloadededFileLocation;
        StopWatch watch = new StopWatch();

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string[] args = Environment.GetCommandLineArgs();

            if (args.Length > 1)
            {
                StartDownload(args[1]);
            }
            else
            {
                MessageBox.Show("No Arguments given! Can't get Download Location", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void StartDownload(string from)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Application (*.exe)|*.exe";
            if (sfd.ShowDialog() == true)
            {
                try
                {
                    watch.Start();
                    if ( File.Exists(sfd.FileName))
                    {
                        File.Delete(sfd.FileName);
                    }
                    WebClient webClient = new WebClient();
                    webClient.DownloadFileCompleted += webClient_DownloadFileCompleted;
                    webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);
                    webClient.DownloadFileAsync(new Uri(from.Replace("\"", "")), sfd.FileName);
                    downloadededFileLocation = sfd.FileName;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Can't Load the File " + Environment.NewLine + "Error: " + ex.ToString(), "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Unavalible Location!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        void webClient_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            watch.Stop();
            MessageBox.Show("Download completed in " + watch.Time + "sek.! Saved to: " + downloadededFileLocation, "Finish!", MessageBoxButton.OK, MessageBoxImage.Information);
            Process.Start(downloadededFileLocation);
            Process.Start("https://bitbucket.org/Jan18101997/bussgeldrechner/commits/all");
            this.Close();
        }

        private void ProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;
        }
    }
}

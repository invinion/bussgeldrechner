﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CatalogCreator
{
    /// <summary>
    /// Interaktionslogik für Category.xaml
    /// </summary>
    public partial class PriceWindow : Window
    {
        public PriceWindow()
        {
            InitializeComponent();
        }

        public PriceWindow(string name, string Remark, string price)
        {
            InitializeComponent();

            description.Text = name;
            punishment.Text = price;
            remark.Text = Remark;
        }

        public string PriceName { get; set; }
        public string PriceRemak { get; set; }
        public string PricePrice { get; set; }

        private void ok_Click(object sender, RoutedEventArgs e)
        {
            int number;
            if (description.Text.Length > 3 && int.TryParse(punishment.Text, out number))
            {
                PriceName = description.Text;
                PriceRemak = remark.Text;
                PricePrice = number.ToString();
                this.DialogResult = true;
            }
            else
            {
                MessageBox.Show("Invalid input! Please check your Data! you have to enter a Price ans a Name!", "Invalid input", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
    }
}

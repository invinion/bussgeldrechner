﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussgeldRechner.Katalog
{
    class KatalogEintrag
    {
        public KatalogEintrag(string name, string xmlLoc, string password)
        {
            Name = name;
            XMLLocation = xmlLoc;
            Password = password;
        }
        public string Name { get; set; }
        public string XMLLocation { get; set; }
        public string Password { get; set; }
    }

    class KatalogList
    {
        public KatalogList()
        {
            KatalogListe = new List<KatalogEintrag>();
        }

        public List<KatalogEintrag> KatalogListe { get; set; }

        public void addToList(string DL, string Name, string PW)
        {
            KatalogListe.Add(new KatalogEintrag(Name, DL, PW));
        }
    }
}

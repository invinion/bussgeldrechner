﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using EncryptStringSample;

namespace CatalogCreator
{
    /// <summary>
    /// Interaktionslogik für GeneratePWWindow.xaml
    /// </summary>
    public partial class GeneratePWWindow : Window
    {
        public GeneratePWWindow()
        {
            InitializeComponent();
        }

        private void inPW_TextChanged(object sender, TextChangedEventArgs e)
        {
            outPW.Text = StringCipher.Encrypt(inPW.Text.Replace(" ", ""), "123456");
        }
    }
}

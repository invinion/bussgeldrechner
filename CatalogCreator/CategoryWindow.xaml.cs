﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CatalogCreator
{
    /// <summary>
    /// Interaktionslogik für CategoryWindow.xaml
    /// </summary>
    public partial class CategoryWindow : Window
    {
        public CategoryWindow()
        {
            InitializeComponent();
        }

        public CategoryWindow(string name)
        {
            InitializeComponent();
            cName.Text = name;
        }

        public string CategoryName { get; set; }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (cName.Text.Length > 2)
            {
                CategoryName = cName.Text;
                this.DialogResult = true;
            }
            else
            {
                MessageBox.Show("Invalid input! Please check your Data! you have to enter a Price ans a Name!", "Invalid input", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
    }
}

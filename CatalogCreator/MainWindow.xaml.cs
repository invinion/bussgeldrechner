﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EncryptStringSample;
using BussgeldRechner.Katalog;
using BussgeldRechner;
using Microsoft.Win32;
using System.Xml;

namespace CatalogCreator
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    /// 

    public class KatalogKategorie
    {
        public KatalogKategorie()
        {
            PreisListe = new List<Preis>();
        }

        public string Name { get; set; }

        public List<Preis> PreisListe;
    }

    public class Preis
    {
        public string Beschreibung { get; set; }
        public string Strafe { get; set; }
        public string Bemerkung { get; set; }
        public string Anzahl { get; set; }
    }

    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void LoadDoc(string xmlLocation)
        {
            try
            {
                if ( Category.Items.Count > 0)
                {
                    int itemCount = Category.Items.Count;
                    for (int i = 0; i < itemCount; i++)
                    {
                        Category.Items.RemoveAt(0);
                    }
                }

                XmlDocument doc = new XmlDocument();
                doc.Load(xmlLocation);

                XmlNode node = doc.SelectSingleNode("/BussgeldKatalog");

                version.Text = node.Attributes["Version"].InnerText;
                creator.Text = node.Attributes["Creator"].InnerText;

                XmlNodeList list = doc.SelectNodes("/BussgeldKatalog/Kategorie");

                foreach (XmlNode item in list)
                {
                    KatalogKategorie kk = new KatalogKategorie();
                    kk.Name = item.Attributes["Name"].InnerText;

                    XmlNodeList preisList = item.ChildNodes;

                    foreach (XmlNode preis in preisList)
                    {
                        kk.PreisListe.Add(new Preis { Beschreibung = preis.Attributes["Beschreibung"].InnerText, Strafe = preis.Attributes["Strafe"].InnerText, Bemerkung = preis.Attributes["Bemerkung"].InnerText, Anzahl = "0" });
                    }

                    Category.Items.Add(kk);
                }

                Category.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to Load File: " + ex.ToString(), "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SaveDoc(string xmlLocation)
        {
            if (Category.Items.Count > 0)
            {
                try
                {
                    XmlWriter doc = null;
                    doc = XmlWriter.Create(xmlLocation);

                    doc.WriteStartDocument();
                    {
                        doc.WriteRaw(Environment.NewLine + Environment.NewLine);
                        doc.WriteComment("Created with BussgeldRechner CatalogCreator");
                        doc.WriteRaw(Environment.NewLine + Environment.NewLine);
                        doc.WriteStartElement("BussgeldKatalog");
                        {
                            doc.WriteAttributeString("Version", version.Text);
                            doc.WriteAttributeString("Date", DateTime.Now.Day + "." + DateTime.Now.Month + "." + DateTime.Now.Year);
                            doc.WriteAttributeString("Creator", creator.Text);

                            for (int i = 0; i < Category.Items.Count; i++)
                            {
                                Category.SelectedIndex = i;
                                KatalogKategorie kk = (KatalogKategorie)Category.SelectedItem;

                                doc.WriteRaw(Environment.NewLine + "  ");
                                doc.WriteStartElement("Kategorie");
                                {
                                    doc.WriteAttributeString("Name", kk.Name);

                                    foreach (Preis item in kk.PreisListe)
                                    {
                                        doc.WriteRaw(Environment.NewLine + "    ");
                                        doc.WriteStartElement("Preis");
                                        {
                                            doc.WriteAttributeString("Beschreibung", item.Beschreibung);
                                            doc.WriteAttributeString("Strafe", item.Strafe);
                                            doc.WriteAttributeString("Bemerkung", item.Bemerkung);
                                        }
                                        doc.WriteEndElement();
                                    }
                                }
                                doc.WriteRaw(Environment.NewLine + "  ");
                                doc.WriteEndElement();
                            }
                        }
                        doc.WriteRaw(Environment.NewLine);
                        doc.WriteEndElement();
                    }
                    doc.WriteEndDocument();

                    doc.Flush();
                    doc.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Unable to create File: " + ex.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Catalog is empty", "Empty", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Category_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int itemCount = Price.Items.Count;
            if ( Category.SelectedIndex != -1)
            {
                for (int i = 0; i < itemCount; i++)
                {
                    Price.Items.RemoveAt(0);
                }

                KatalogKategorie kk = (KatalogKategorie)Category.SelectedItem;

                if (kk.PreisListe.Count > 0)
                {
                    foreach (Preis item in kk.PreisListe)
                    {
                        Price.Items.Add(new Preis { Beschreibung = item.Beschreibung, Anzahl = item.Anzahl, Bemerkung = item.Bemerkung, Strafe = item.Strafe });
                    }
                }
            }
            else
            {
                for (int i = 0; i < itemCount; i++)
                {
                    Price.Items.RemoveAt(0);
                }
            }
        }

        private void delCategory_Click(object sender, RoutedEventArgs e)
        {
            Category.Items.RemoveAt(Category.SelectedIndex);
        }

        private void delPrice_Click(object sender, RoutedEventArgs e)
        {
            Price.Items.RemoveAt(Price.SelectedIndex);

            KatalogKategorie kk = (KatalogKategorie)Category.SelectedItem;
            List<Preis> pList = new List<Preis>();

            for (int i = 0; i < Price.Items.Count; i++)
            {
                Price.SelectedIndex = i;
                Preis p = (Preis)Price.SelectedItem;
                pList.Add(p);
            }

            kk.PreisListe = pList;

            Category.Items.Refresh();
        }

        private void open_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "BussgeldRechner File | *.xml";
            if (ofd.ShowDialog() == true)
            {
                LoadDoc(ofd.FileName);
            }
        }

        private void save_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "BussgeldRechner File | *.xml";
            if (sfd.ShowDialog() == true)
            {
                SaveDoc(sfd.FileName);
            }
        }

        private void addCategory_Click(object sender, RoutedEventArgs e)
        {
            CategoryWindow cw = new CategoryWindow();
            if (cw.ShowDialog() == true)
            {
                Category.Items.Add(new KatalogKategorie { Name = cw.CategoryName});
            }
        }

        private void changeCategory_Click(object sender, RoutedEventArgs e)
        {
            if (Category.SelectedIndex != -1)
            {
                CategoryWindow cw = new CategoryWindow();
                if (cw.ShowDialog() == true)
                {
                    KatalogKategorie kk = (KatalogKategorie)Category.SelectedItem;
                    kk.Name = cw.CategoryName;
                    Category.SelectedItem = kk;
                    Category.Items.Refresh();
                }
            }
        }

        private void addPrice_Click(object sender, RoutedEventArgs e)
        {
            if (Category.SelectedIndex != -1)
            {
                PriceWindow c = new PriceWindow();
                if (c.ShowDialog() == true)
                {
                    KatalogKategorie kk = (KatalogKategorie)Category.SelectedItem;
                    kk.PreisListe.Add(new Preis { Beschreibung = c.PriceName, Bemerkung = c.PriceRemak, Strafe = c.PricePrice });
                    Category.SelectedItem = kk;
                    Category.Items.Refresh();
                    int lastIndex = Category.SelectedIndex;
                    Category.SelectedIndex = -1;
                    Category.SelectedIndex = lastIndex;
                }
            }
        }

        private void changePrice_Click(object sender, RoutedEventArgs e)
        {
            if (Category.SelectedIndex != -1 && Price.SelectedIndex != -1)
            {
                PriceWindow c = new PriceWindow();
                if (c.ShowDialog() == true)
                {
                    KatalogKategorie kk = (KatalogKategorie)Category.SelectedItem;
                    Preis p = (Preis)Price.SelectedItem;
                    p.Beschreibung = c.PriceName;
                    p.Bemerkung = c.PriceRemak;
                    p.Strafe = c.PricePrice;
                    Price.SelectedItem = p;
                    Category.SelectedItem = kk;
                    Category.Items.Refresh();
                }
            }
        }

        private void generatePW_Click(object sender, RoutedEventArgs e)
        {
            new GeneratePWWindow().Show();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Xml;
using System.Diagnostics;
using Jan18101997.Utility;
using EncryptStringSample;
using BussgeldRechner.Katalog;

namespace BussgeldRechner
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    
    public class KatalogKategorie
    {
        public KatalogKategorie()
        {
            PreisListe = new List<Preis>();
        }

        public string Name { get; set; }

        public List<Preis> PreisListe;
    }

    public class Preis
    {
        public string Beschreibung { get; set; }
        public string Strafe { get; set; }
        public string Bemerkung { get; set; }
        public string Anzahl { get; set; }
    }

    public class ListeVerstoesseItem
    {
        public string Kategorie { get; set; }
        public string Beschreibung { get; set; }
        public string EinzelPreis { get; set; }
        public string Anzahl { get; set; }
        public string PreisGesamt { get; set; }
    }

    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            InitializeLanguage();
            InitializeUpdater();
            InitializeKatalogList();

            selectList.SelectedIndex = Properties.Settings.Default.LasteSelectedIndex;
        }

        string Version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        Update Updater;
        KatalogList Katalog;
        int lastselected;

        #region Methoden
        private void InitializeUpdater()
        {
            Updater = new Update(@"http://jan18101997.bplaced.net/BussgeldRechner/UpdateList.xml");
            Updater.UpdateE += Updater_UpdateE;
            Updater.UpdateDE += Updater_UpdateDE;
            Updater.StartCheck();
        }

        private void InitializeLanguage()
        {
            ResourceDictionary dict = new ResourceDictionary();

            if (Environment.GetCommandLineArgs().Length == 2 && Environment.GetCommandLineArgs()[1] == "noLang")
            {
                dict.Source = new Uri("..\\Resources\\StringResources.null.xaml", UriKind.Relative);
            }
            else if (Environment.GetCommandLineArgs().Length == 2 && Environment.GetCommandLineArgs()[1] == "en-US")
            {
                dict.Source = new Uri("..\\Resources\\StringResources.en-US.xaml", UriKind.Relative);
            }
            else
            {
                switch (Thread.CurrentThread.CurrentCulture.ToString())
                {
                    case "de-DE":
                        dict.Source = new Uri("..\\Resources\\StringResources.de-DE.xaml", UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("..\\Resources\\StringResources.en-US.xaml", UriKind.Relative);
                        break;
                }
            }
            this.Resources.MergedDictionaries.Add(dict);
        }

        public void InitializeKatalogList()
        {
            Katalog = new KatalogList();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load("http://jan18101997.bplaced.net/BussgeldRechner/Listen/Listen.xml");

                XmlNodeList list = doc.SelectNodes("/BussgeldRechner/Server");

                foreach (XmlNode item in list)
                {
                    Katalog.addToList(item.Attributes["Download"].InnerText, item.Attributes["Name"].InnerText, item.Attributes["Password"].InnerText);
                }

                foreach (KatalogEintrag item in Katalog.KatalogListe)
                {
                    selectList.Items.Add(new ComboBoxItem().Content = item.Name);
                }

                selectList.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(this.FindResource("LoadingXMLList_Error").ToString().Replace("%ex", ex.ToString()), this.FindResource("error").ToString(), MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private string getURLbyName(string name)
        {
            foreach (KatalogEintrag item in Katalog.KatalogListe)
            {
                if (item.Name == selectList.SelectedItem.ToString())
                {
                    return item.XMLLocation;
                }
            }

            return "False";
        }

        private string getPWbyName(string name)
        {
            foreach (KatalogEintrag item in Katalog.KatalogListe)
            {
                if (item.Name == selectList.SelectedItem.ToString())
                {
                    if (item.Password.Length != 0)
                    {
                        return StringCipher.Decrypt(item.Password, "123456");
                    }
                    else
                    {
                        return "";
                    }
                }
            }

            return "False";
        }

        private void LoadDoc(string xmlLocation)
        {
            if (CategoryList.Items.Count > 0)
            {
                int count = CategoryList.Items.Count;
                for (int i = 0; i < count; i++)
                {
                    CategoryList.Items.RemoveAt(0);
                }
            }

            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(xmlLocation);

                XmlNode node = doc.SelectSingleNode("/BussgeldKatalog");

                fileInfo.Content = this.FindResource("versionDate").ToString().Replace("%v", node.Attributes["Version"].InnerText).Replace("%d", node.Attributes["Date"].InnerText).Replace("%c", node.Attributes["Creator"].InnerText);

                XmlNodeList list = doc.SelectNodes("/BussgeldKatalog/Kategorie");

                foreach (XmlNode item in list)
                {
                    KatalogKategorie kk = new KatalogKategorie();
                    kk.Name = item.Attributes["Name"].InnerText;

                    XmlNodeList preisList = item.ChildNodes;

                    foreach (XmlNode preis in preisList)
                    {
                        kk.PreisListe.Add(new Preis { Beschreibung = preis.Attributes["Beschreibung"].InnerText, Strafe = preis.Attributes["Strafe"].InnerText, Bemerkung = preis.Attributes["Bemerkung"].InnerText, Anzahl = "0" });
                    }

                    CategoryList.Items.Add(kk);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this.FindResource("cantloadxml").ToString() + ex, this.FindResource("error").ToString());
            }

        }

        #endregion

        #region Updater Event
        private void Updater_UpdateE(object sender, UpdateEventArgs e)
        {
            if (e.sucsuccess == true && Utility.CompareVersions(Version, e.newVersion) == 1)
            {
                if (MessageBox.Show(this.FindResource("newVersionText").ToString().Replace("%v", e.newVersion).Replace("%cv", Version), this.FindResource("newVersion").ToString(), MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes, MessageBoxOptions.DefaultDesktopOnly) == MessageBoxResult.Yes)
                {
                    Updater.DownlodandUpdate();
                }
            }
            else if (e.sucsuccess == true && Utility.CompareVersions(Version, e.newVersion) == -1)
            {
                Debug.WriteLine("Current Version is newer then the Downloadebal Version");
            }
        }

        private void Updater_UpdateDE(object sender, UpdateDownloadEventArgs e)
        {
            if (e.sucsuccess == true)
            {
                Process.Start(e.updaterLoc, "\"" + e.NewFileLocation + "\"");
                Environment.Exit(-1);
            }
            else
            {
                MessageBox.Show(this.FindResource("updaterNotLoadedError").ToString().Replace("%ex", e.ex.ToString()), this.FindResource("error").ToString(), MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK, MessageBoxOptions.DefaultDesktopOnly);
            }
        }
        #endregion

        #region Klick Event
        private void ReloadXML_Click(object sender, RoutedEventArgs e)
        {
            if (selectList.SelectedIndex != -1)
            {
                LoadDoc(getURLbyName(selectList.SelectedItem.ToString()));
            }
        }

        private void CalcPrice_Click(object sender, RoutedEventArgs e)
        {
            if (CategoryList.Items.Count > 0)
            {
                if (ViolationsList.Items.Count > 0)
                {
                    int count = ViolationsList.Items.Count;
                    for (int i = 0; i < count; i++)
                    {
                        ViolationsList.Items.RemoveAt(0);
                    }
                }

                int lastindexKategorie = CategoryList.SelectedIndex;
                int lastindexPreisList = PriceList.SelectedIndex;
                int Preis = 0;
                int Verstoesse = 0;

                for (int i = 0; i < CategoryList.Items.Count; i++)
                {
                    CategoryList.SelectedIndex = i;
                    KatalogKategorie kk = (KatalogKategorie)CategoryList.SelectedItem;
                    if (kk.PreisListe != null && kk.PreisListe.Count > 0)
                    {
                        foreach (Preis item in kk.PreisListe)
                        {
                            if (int.Parse(item.Anzahl) > 0)
                            {
                                ViolationsList.Items.Add(new ListeVerstoesseItem { Kategorie = kk.Name, Beschreibung = item.Beschreibung, EinzelPreis = item.Strafe, Anzahl = item.Anzahl, PreisGesamt = (int.Parse(item.Strafe) * int.Parse(item.Anzahl)).ToString() });
                                Verstoesse += int.Parse(item.Anzahl);
                                Preis += (int.Parse(item.Strafe) * int.Parse(item.Anzahl));
                            }
                        }
                    }
                }
                TotalPrice.Text = Preis.ToString();
                TotalViolationsCount.Text = Verstoesse.ToString();
                CategoryList.SelectedIndex = lastindexKategorie;
                PriceList.SelectedIndex = lastindexPreisList;
            }
        }

        private void BugReport_Click(object sender, RoutedEventArgs e)
        {
            new BugReport().ShowDialog();
        }

        private void Twitter_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Process.Start("https://twitter.com/intent/tweet?text=" + this.FindResource("twitterMessage").ToString().Replace("%n", "@Jan18101997"));
        }

        private void About_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            System.Diagnostics.Process.Start("http://invinion.eu/");
        }

        private void createOwn_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("http://jan18101997.bplaced.net/BussgeldRechner/CatalogCreator.exe");
        }
        #endregion

        #region SelectionChanged Event
        private void CategoryList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CategoryList.Items.Count > 0)
            {
                KatalogKategorie kk = (KatalogKategorie)CategoryList.SelectedItem;

                int count = PriceList.Items.Count;
                for (int i = 0; i < count; i++)
                {
                    PriceList.Items.RemoveAt(0);
                }

                if (kk != null && kk.PreisListe.Count > 0)
                {
                    foreach (Preis item in kk.PreisListe)
                    {
                        PriceList.Items.Add(new Preis { Beschreibung = item.Beschreibung, Strafe = item.Strafe, Bemerkung = item.Bemerkung, Anzahl = item.Anzahl });
                    }
                }
            }
            else
            {
                int count = PriceList.Items.Count;
                for (int i = 0; i < count; i++)
                {
                    PriceList.Items.RemoveAt(0);
                }
            }
        }

        private void selectList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (selectList.SelectedIndex != -1)
            {
                if (getPWbyName(selectList.SelectedItem.ToString()).Length > 0)
                {
                    CheckPassword cp = new CheckPassword(getPWbyName(selectList.SelectedItem.ToString()));
                    if (cp.ShowDialog() == true)
                    {
                        LoadDoc(getURLbyName(selectList.SelectedItem.ToString()));
                        CalcPrice_Click(this, new RoutedEventArgs());
                    }
                    else
                    {
                        selectList.SelectedIndex = lastselected;
                    }
                }
                else
                {
                    LoadDoc(getURLbyName(selectList.SelectedItem.ToString()));
                    CalcPrice_Click(this, new RoutedEventArgs());
                }
            }
            lastselected = selectList.SelectedIndex;
        }

        private void PriceList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Preis preis = (Preis)PriceList.SelectedItem;
            if (preis != null)
            {
                ViolationsCount.Text = preis.Anzahl;
            }
        }
        #endregion

        #region Preview Event
        private void ViolationsCount_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Preis preis = (Preis)PriceList.SelectedItem;
            int OUT;
            if (preis != null && int.TryParse(e.Text, out OUT) == true)
            {
                preis.Anzahl = OUT.ToString();
                PriceList.Items.Refresh();

                KatalogKategorie kk = (KatalogKategorie)CategoryList.SelectedItem;
                PriceList.SelectionChanged -= PriceList_SelectionChanged;
                List<Preis> pl = new List<Preis>();
                int lastindex = PriceList.SelectedIndex;
                for (int i = 0; i < PriceList.Items.Count; i++)
                {
                    PriceList.SelectedIndex = i;
                    Preis p = (Preis)PriceList.SelectedItem;
                    pl.Add(new Preis { Beschreibung = p.Beschreibung, Bemerkung = p.Bemerkung, Anzahl = p.Anzahl, Strafe = p.Strafe });
                }
                PriceList.SelectedIndex = lastindex;
                kk.PreisListe = pl;
                PriceList.SelectionChanged += PriceList_SelectionChanged;
            }
            else
            {
                e.Handled = false;
            }
        }
        #endregion
    }
}

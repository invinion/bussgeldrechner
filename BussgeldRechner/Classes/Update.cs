﻿using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Xml;

namespace BussgeldRechner
{
    class UpdateEventArgs : EventArgs
    {
        public UpdateEventArgs(bool Updatesucsuccess, string NewVersion)
        {
            sucsuccess = Updatesucsuccess;
            newVersion = NewVersion;
        }

        public bool sucsuccess { get; set; }
        public Exception ex { get; set; }
        public string newVersion { get; set; }
    }

    class UpdateDownloadEventArgs : EventArgs
    {
        public UpdateDownloadEventArgs(bool Updatesucsuccess, string updaterLocation, string NFV)
        {
            sucsuccess = Updatesucsuccess;
            updaterLoc = updaterLocation;
            NewFileLocation = NFV;
        }

        public UpdateDownloadEventArgs(bool Updatesucsuccess, string updaterLocation, Exception error)
        {
            sucsuccess = Updatesucsuccess;
            updaterLoc = updaterLocation;
            ex = error;
        }

        public bool sucsuccess { get; set; }
        public string updaterLoc { get; set; }
        public string NewFileLocation { get; set; }
        public Exception ex { get; set; }
    }

    class Update
    {
        public Update(string updateFileLocation)
        {
            fileLoc = updateFileLocation;
            checkForUpdate = new Thread(checkUpdate);
            checkForUpdate.IsBackground = true;
        }

        Thread checkForUpdate;
        string fileLoc;
        string newversionFile;
        string updaterURL;
        public event UpdateEvent UpdateE;
        public delegate void UpdateEvent(object sender, UpdateEventArgs e);
        public event UpdateDownloadEvent UpdateDE;
        public delegate void UpdateDownloadEvent(object sender, UpdateDownloadEventArgs e);

        public void StartCheck()
        {
            checkForUpdate.Start();
        }

        public void DownlodandUpdate()
        {
            new Thread(loadUpdater).Start();
        }

        private void loadUpdater()
        {
            try
            {
                if (Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Jan18101997\\BussgeldRechner") == false)
                {
                    Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Jan18101997\\BussgeldRechner");
                }

                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Jan18101997\\BussgeldRechner\\updater.exe") == true)
                {
                    File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Jan18101997\\BussgeldRechner\\updater.exe");
                }

                WebClient loadFile = new WebClient();
                loadFile.DownloadFile(updaterURL, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Jan18101997\\BussgeldRechner\\updater.exe");

                UpdateDE(this, new UpdateDownloadEventArgs(true, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Jan18101997\\BussgeldRechner\\updater.exe", newversionFile));
            }
            catch (Exception ex) { UpdateDE(this, new UpdateDownloadEventArgs(true, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Jan18101997\\BussgeldRechner\\updater.exe", ex)); }
        }

        private void checkUpdate()
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(fileLoc);

                string ver = doc.SelectSingleNode("/BussgeldRechner/version").Attributes["Version"].InnerText;
                updaterURL = doc.SelectSingleNode("/BussgeldRechner/updaterLocation").Attributes["URL"].InnerText;
                newversionFile = doc.SelectSingleNode("/BussgeldRechner/currentVersionLocation").Attributes["URL"].InnerText;

                UpdateEventArgs uea = new UpdateEventArgs(true, ver);
                UpdateE(this, uea);
            }
            catch (Exception ex)
            {
                UpdateEventArgs uea = new UpdateEventArgs(false, "-1");
                uea.ex = ex;
                UpdateE(this, uea);
            }
        }
    }
}

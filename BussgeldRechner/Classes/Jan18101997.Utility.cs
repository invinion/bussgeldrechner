﻿using System;
using System.Text.RegularExpressions;

namespace Jan18101997.Utility
{
    static class Utility
    {
        /// <summary>
        /// Compares two VersionStrings
        /// </summary>
        /// <param name="currentVersion">Version String of the CurrentVersion</param>
        /// <param name="newVersion">Version String of the new Version</param>
        /// <returns>retuns 0 if versions are equal retuns 1 if versions is newer retuns -1 if versions is older</returns>
        public static int CompareVersions(string currentVersion, string newVersion)
        {
            int[] newV = ConvertVersionStringToIntArr(newVersion);
            int[] currentV = ConvertVersionStringToIntArr(currentVersion);

            for (int i = 0; i < newV.Length; i++)
            {
                if (newV[i] > currentV[i])
                {
                    return 1;
                }
                if (newV[i] < currentV[i])
                {
                    return -1;
                }
            }

            return 0;
        }

        /// <summary>
        /// Converts a Version string to a int arr with seperat parts
        /// </summary>
        /// <param name="VersionString"></param>
        /// <returns>int Arr</returns>
        public static int[] ConvertVersionStringToIntArr(string VersionString)
        {
            if (IsVersionString(VersionString) == false)
            {
                throw new ArgumentException("Please enter a valid version number!");
            }

            string[] VersionSections = VersionString.Split('.');
            int[] VersionSectionsInt = new int[VersionSections.Length];
            for (int i = 0; i < VersionSections.Length; i++)
            {
                VersionSectionsInt[i] = int.Parse(VersionSections[i]);
            }

            return VersionSectionsInt;
        }

        /// <summary>
        /// Checks if the String is a valid version string
        /// </summary>
        /// <param name="versionString">Version string to check</param>
        /// <returns>if the sting is a valid version string</returns>
        public static bool IsVersionString(string versionString)
        {
            return Regex.IsMatch(versionString, @"\d{1,9}\.\d{1,9}\.\d{1,9}\.\d{1,9}");
        }
    }
}

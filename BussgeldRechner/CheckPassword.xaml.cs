﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;

namespace BussgeldRechner
{
    /// <summary>
    /// Interaktionslogik für CheckPassword.xaml
    /// </summary>
    public partial class CheckPassword : Window
    {
        public CheckPassword(string pw)
        {
            InitializeComponent();
            InitializeLanguage();

            password = pw;
        }

        string password;
        bool allow = false;

        private void InitializeLanguage()
        {
            ResourceDictionary dict = new ResourceDictionary();

            if (Environment.GetCommandLineArgs().Length == 2 && Environment.GetCommandLineArgs()[1] == "noLang")
            {
                dict.Source = new Uri("..\\Resources\\StringResources.null.xaml", UriKind.Relative);
            }
            else if (Environment.GetCommandLineArgs().Length == 2 && Environment.GetCommandLineArgs()[1] == "en-US")
            {
                dict.Source = new Uri("..\\Resources\\StringResources.en-US.xaml", UriKind.Relative);
            }
            else
            {
                switch (Thread.CurrentThread.CurrentCulture.ToString())
                {
                    case "de-DE":
                        dict.Source = new Uri("..\\Resources\\StringResources.de-DE.xaml", UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("..\\Resources\\StringResources.en-US.xaml", UriKind.Relative);
                        break;
                }
            }
            this.Resources.MergedDictionaries.Add(dict);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if ( pw.Password.Replace(" ", "") == password.Replace(" ", ""))
            {
                allow = true;
                this.DialogResult = true;
            }
            else
            {
                if (MessageBox.Show(this.FindResource("PW_Wrong").ToString(), this.FindResource("error").ToString(), MessageBoxButton.OKCancel, MessageBoxImage.Information) == MessageBoxResult.Cancel)
                {
                    this.DialogResult = false;
                }
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (allow == false)
            {
                this.DialogResult = false;
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if ( e.Key == Key.Enter)
            {
                Button_Click(this, new RoutedEventArgs());
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;
using System.Net;
using System.Net.Mail;

namespace BussgeldRechner
{
    /// <summary>
    /// Interaktionslogik für BugReport.xaml
    /// </summary>
    public partial class BugReport : Window
    {
        public BugReport()
        {
            InitializeComponent();
            InitializeLanguage();
            genCAPTCHA();
        }

        private void genCAPTCHA()
        {
            String allowchar = " ";
            allowchar = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z";
            allowchar += "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,y,z";
            allowchar += "1,2,3,4,5,6,7,8,9,0";
            char[] a = { ',' };
            String[] ar = allowchar.Split(a);
            String pwd = " ";
            string temp = " ";
            Random r = new Random();

            for (int i = 0; i < 8; i++)
            {
                temp = ar[(r.Next(0, ar.Length))];

                pwd += temp;
            }

            CAPTCHALabel.Content = pwd;
        }

        private void InitializeLanguage()
        {
            ResourceDictionary dict = new ResourceDictionary();

            if (Environment.GetCommandLineArgs().Length == 2 && Environment.GetCommandLineArgs()[1] == "noLang")
            {
                dict.Source = new Uri("..\\Resources\\StringResources.null.xaml", UriKind.Relative);
            }
            else if (Environment.GetCommandLineArgs().Length == 2 && Environment.GetCommandLineArgs()[1] == "en_US")
            {
                dict.Source = new Uri("..\\Resources\\StringResources.en-US.xaml", UriKind.Relative);
            }
            else
            {
                switch (Thread.CurrentThread.CurrentCulture.ToString())
                {
                    case "de-DE":
                        dict.Source = new Uri("..\\Resources\\StringResources.de-DE.xaml", UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("..\\Resources\\StringResources.en-US.xaml", UriKind.Relative);
                        break;
                }
            }
            this.Resources.MergedDictionaries.Add(dict);
        }

        private void send_Click(object sender, RoutedEventArgs e)
        {
            if (CAPTCHALabel.Content.ToString().Replace(" ", "") == CAPTCHA.Text.Replace(" ", "") && absender.Text.Length != 0 && beschreibung.Text.Length != 0 && grund.Text.Length != 0)
            {
                try
                {
                    MailMessage mail = new MailMessage();
                    SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                    mail.From = new MailAddress(BussgeldRechner.Private.Private.mailARD);
                    mail.To.Add("jan181097@gmail.com");
                    mail.Subject = "[BugReport BussgeldRechner] " + grund.Text;
                    mail.Body = "Von: " + absender.Text + Environment.NewLine + beschreibung.Text;

                    SmtpServer.Port = 587;
                    SmtpServer.Credentials = new System.Net.NetworkCredential("jan181097", BussgeldRechner.Private.Private.mailPW);
                    SmtpServer.EnableSsl = true;

                    SmtpServer.Send(mail);
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(this.FindResource("BR_error").ToString().Replace("%ex", ex.ToString()), this.FindResource("error").ToString(), MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show(this.FindResource("BR_wrong").ToString(), this.FindResource("error").ToString(), MessageBoxButton.OK, MessageBoxImage.Exclamation);
                genCAPTCHA();
            }
        }
    }
}
